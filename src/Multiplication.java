public class Multiplication implements Operations {
    @Override
    public double calculatorOutput(double leftOperand, double rightOperand) {
        return leftOperand * rightOperand;
    }
}