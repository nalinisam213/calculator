import java.util.HashMap;
import java.util.Map;

public class Caluculator {
    private char operationToPerform;
    private double leftoperand;
    private double rightoperand;

    private Map<Character, Operations> operationsMap = new HashMap<>();

    public Caluculator() {
        operationsMap.put('+', new Addition());
        operationsMap.put('-', new Subtraction());
        operationsMap.put('*', new Multiplication());
        operationsMap.put('/', new Division());
        operationsMap.put('%', new Modulo());
    }

    public double makeCalculation(double leftoperand, double rightoperand, char operation) {
        this.leftoperand = leftoperand;
        this.rightoperand = rightoperand;
        this.operationToPerform = operation;
        if (operationsMap.containsKey(operationToPerform)) {
            Operations operationMapValue = operationsMap.get(operationToPerform);
            return operationMapValue.calculatorOutput(leftoperand, rightoperand);
        }
        else
            {
            System.out.println("Invalid operation to Perform");
            return 0;
        }
    }
}
