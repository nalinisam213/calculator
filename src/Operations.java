public interface Operations {
    double calculatorOutput(double leftOperand, double rightOperand);
}
